package hw2;

public class Main {

    public static void main(String[] args) {
        Recipient recipient = new Recipient("Олег", "Булаткин", "Сергеевич", "23175", "10120", 120_000);

        Sender sender = new Sender("Василий", "Иванов", "Александрович", "15825", "44321", 90_000);

        Transfer transferAccToAcc = new TransferAccToAcc(120_000, Currency.RUB, recipient, sender);
        Transfer transferAccToAcc1 = new TransferAccToAcc(20_000, Currency.RUB, recipient, sender);
        Transfer transferCardToCard = new TransferCardToCard(45_000, Currency.EUR, recipient, sender);

        transferAccToAcc.doTransfer();
        transferAccToAcc1.doTransfer();
        transferCardToCard.doTransfer();

        System.out.println(sender.getLowerCaseFullName());
        System.out.println(recipient.getUpperCaseFullName());
        System.out.println(recipient.getFullName());
    }


}

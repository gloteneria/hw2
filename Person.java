package hw2;

import java.util.Locale;

public class Person {
    private String name;
    private String lastName;
    private String patronymic;
    private String cardNumber;
    private String accountNumber;
    private double amountOfFunds;


    public Person(String name, String last_name, String patronymic, String card_number, String account_number, double amount_of_funds) {
        this.name = name;
        this.lastName = last_name;
        this.patronymic = patronymic;
        this.cardNumber = card_number;
        this.accountNumber = account_number;
        this.amountOfFunds = amount_of_funds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getAmountOfFunds() {
        return amountOfFunds;
    }

    public void setAmountOfFunds(double amountOfFunds) {
        this.amountOfFunds = amountOfFunds;
    }



    public String getLowerCaseFullName() {
        return getFullName().toLowerCase(Locale.ROOT);

    }

    public String getUpperCaseFullName() {
        return getFullName().toUpperCase(Locale.ROOT);
    }

    public String getFullName(){
        return name + " " + lastName + " " + patronymic;
    }

}

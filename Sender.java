package hw2;

import java.util.Locale;

public class Sender extends Person{

    public Sender(String name, String last_name, String patronymic, String card_number, String account_number, double amount_of_funds) {
        super(name, last_name, patronymic, card_number, account_number, amount_of_funds);
    }
}

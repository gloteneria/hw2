package hw2;

import java.time.LocalDateTime;
import java.util.Objects;

public class Transfer {
    private long transferNumber;
    private double sum;
    private Currency currency;
    private Recipient recipient;
    private Sender sender;
    private LocalDateTime transferDate;
    private static int count = 0;

    public long getTransferNumber() {
        return transferNumber;
    }

    public void setTransferNumber(long transferNumber) {
        this.transferNumber = transferNumber;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public LocalDateTime getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(LocalDateTime transferDate) {
        this.transferDate = transferDate;
    }

    public static int getCount() {
        return count;
    }

    public Transfer(double sum, Currency currency, Recipient recipient, Sender sender) {
        this.transferNumber = count++;
        this.sum = sum;
        this.currency = currency;
        this.recipient = recipient;
        this.sender = sender;
        this.transferDate = LocalDateTime.now();
    }

    public void doTransfer(){
        if (sender.getAmountOfFunds() >= sum){
            System.out.println("Перевод выполнен успешно");
            System.out.println(this);
        }
        else {
            System.out.println("Перевод совершить нельзя, недостаточно средств");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return transferNumber == transfer.transferNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(transferNumber);
    }

    @Override
    public String toString() {
        return "Номер перевода: " + getTransferNumber() +
                ", Дата: " + getTransferDate() +
                ", Валюта: " + getCurrency() +
                ", Сумма: " + getSum() +
                ", Получатель: " + getRecipient().getFullName() +
                ", Отправитель: " + getSender().getFullName();
    }
}

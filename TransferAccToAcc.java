package hw2;

public class TransferAccToAcc extends Transfer{


    public TransferAccToAcc(double sum, Currency currency, Recipient recipient, Sender sender) {
        super(sum, currency, recipient, sender);
    }

    @Override
    public String toString() {
        return super.toString() +
                ", Номер счета получателя: " + getRecipient().getAccountNumber() +
                ", Номер счета отправителя: " + getSender().getAccountNumber();
    }


}

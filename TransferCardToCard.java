package hw2;

public class TransferCardToCard extends Transfer{


    public TransferCardToCard(double sum, Currency currency, Recipient recipient, Sender sender) {
        super(sum, currency, recipient, sender);
    }

    @Override
    public String toString() {
        return super.toString() +
                ", Номер карты получателя: " + getRecipient().getCardNumber() +
                ", Номер карты отправителя: " + getSender().getCardNumber();
    }
}
